terraform {
  required_version = ">= 0.11.8"

  backend "s3" {
    bucket         = "securitygroup123"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}

provider "aws" {
  region     = "us-east-1"
  shared_credentials_file = "/var/lib/jenkins/.aws/credentials"
  profile                 = "default"
}
