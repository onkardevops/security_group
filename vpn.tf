################### For Luxoft ###################

resource "aws_security_group_rule" "OpenVPN_Luxoft" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["195.90.110.113/32", "195.90.110.125/32", 
                       "94.177.7.1/32", "195.90.110.80/32"]
  description       = "OpenVPN Luxoft"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Luxoft_Sonia" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["89.43.179.249/32", "81.196.3.251/32"]
  description       = "OpenVPN Luxoft Sonia"
  security_group_id = "sg-0663e1293f58b7168"
}

################### For Incedo ###################

resource "aws_security_group_rule" "OpenVPN_Incedo" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["14.141.217.198/32", "14.142.129.6/32"]
  description       = "OpenVPN Incedo"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Gurugram_Team" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["106.215.191.64/32", "160.202.38.104/32"]
  description       = "OpenVPN Incedo Gurugram Team"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Gavish" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["43.224.156.43/32"]
  description       = "OpenVPN Incedo Gavish"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Deepak" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["27.97.43.231/32"]
  description       = "OpenVPN Incedo Deepak"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Ankit" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["123.201.194.111/32"]
  description       = "OpenVPN Incedo Ankit"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Sudhanshu" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["103.103.56.200/32"]
  description       = "OpenVPN Incedo Sudhanshu"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Amit" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["47.8.141.88/32"]
  description       = "OpenVPN Incedo Amit"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Vipin" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["103.217.117.190/32"]
  description       = "OpenVPN Incedo Vipin"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Venkat" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["157.47.68.119/32"]
  description       = "OpenVPN Incedo Venkat"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Praveen" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["47.30.209.174/32"]
  description       = "OpenVPN Incedo Praveen"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Onkar" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["106.210.207.184/32"]
  description       = "OpenVPN Incedo Onkar"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Ankur" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["123.201.52.217/32"]
  description       = "OpenVPN Incedo Ankur"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Minal" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["103.109.13.159/32"]
  description       = "OpenVPN Incedo Minal"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "OpenVPN_Incedo_Kiran" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["123.201.52.98/32"]
  description       = "OpenVPN Incedo Kiran"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "Incedo_Mobile_VPN" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["106.210.96.229/32", "150.242.72.203/32", 
                      "110.225.91.135/32", "160.202.39.232/32", 
                      "165.225.124.113/32", "223.225.82.48/32"]
  description       = "Incedo Mobile VPN"
  security_group_id = "sg-0663e1293f58b7168"
}

resource "aws_security_group_rule" "Incedo_VPN_Puneet_Mobile_team" {
  type              = "ingress"
  from_port         = 1194
  to_port           = 1194
  protocol          = "udp"
  cidr_blocks       = ["103.46.201.197/32"]
  description       = "Incedo VPN Puneet Mobile team"
  security_group_id = "sg-0663e1293f58b7168"
}
